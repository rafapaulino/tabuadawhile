﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TabuadaWhile
{
    class Program
    {
        static void Main(string[] args)
        {
            int numeroDigitado;
            int i = 1;
            Console.Write("Digite um número: ");
            numeroDigitado = int.Parse(Console.ReadLine());

            while (i <= 10)
            {
                Console.WriteLine(numeroDigitado + " x " + i + " = " + (numeroDigitado * i));
                i++;
            }

            Console.ReadKey();
        }
    }
}
